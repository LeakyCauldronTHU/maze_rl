from q_learning_agent import Q_Learning
from sarsa_agent import Sarsa
from maze_env import Maze
import sys
import copy
import threading
from utils import cmd_visualize
import numpy as np
import time
from arg_parser import common_parser

if __name__ == "__main__":
    sys_args = sys.argv
    env = Maze()
    args_parse = common_parser()
    args, _ = args_parse.parse_known_args(sys_args)
    output_args = copy.deepcopy(args.__dict__)
    print("-------------- args ---------------")
    for k in output_args:
        print('{} = {}'.format(k, output_args[k]))
    print("-----------------------------------")

    assert args.alg in ['q_learning', 'sarsa'], 'Illegal algorithm name found, exit'
    output_args.pop('alg')
    output_args.pop('visualize')
    output_args.pop('save_video')
    output_args.pop('evaluate')

    if args.evaluate:
        # evaluate phase
        env = Maze()
        if args.alg == 'q_learning':
            agent = Q_Learning(env=env, path=args.path, num_episodes=args.num_episodes, epsilon=args.epsilon, epoch_gap=None or args.visualize)
        else:
            agent = Sarsa(env=env, path=args.path, epoch_gap=None or args.visualize)
        agent.load()
        values = agent.get_value_table()
        action_values = agent.get_action_value_table()
        # print("------value table------")
        # print(values)
        # print("-----action-value table-----")
        # print(action_values)

        state = env.reset()
        while True:
            action = agent.select_action(state, is_test=True)
            # print("state", state, "action", action)
            env.render(time_gap=3)
            nxt_state, reward, done = env.step(action)
            state = copy.deepcopy(nxt_state)
            if done:
                env.render(time_gap=3)
                break
    else:
        # training phase
        if args.visualize:
            np.save(file='./render/terminate_file.npy', arr=np.array([0]))
            th = threading.Thread(target=cmd_visualize, args=())
            th.start()
            time.sleep(2)

        if args.alg == 'q_learning':
            print("---------------------- q learning ---------------------")
            # agent = Q_Learning(env=env, path=args.path, num_episodes=args.num_episodes,
            # epsilon=args.epsilon, epoch_gap=None or args.visualize)
            agent = Q_Learning(env=env, epoch_gap=None or args.visualize, **output_args)
        else:
            agent = Sarsa(env=env, path=args.path, epoch_gap=None or args.visualize)

        agent.train()

        if args.visualize:
            np.save(file='./render/terminate_file.npy', arr=np.array([1]))
            th.join()


