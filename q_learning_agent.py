import numpy as np
from value_table import State_Values, State_Action_Values
import time


class Q_Learning:
    def __init__(self, env=None, alpha=0.01, episode_length=100, num_episodes=10000,
                 path=None, gamma=0.99, epsilon=0.3, epoch_gap=None):
        print("alpha={}, episode_length={}, num_episodes={}, gamma={}, epsilon={}".format(alpha, episode_length,
                                                                                          num_episodes, gamma, epsilon))
        np.random.seed(0)
        self.env = env
        self.values = State_Values(self.env.state_dims)
        self.action_values = State_Action_Values(self.env.state_dims, self.env.num_actions)

        self.alpha = alpha
        self.episode_length = episode_length
        self.num_episodes = num_episodes
        self.path = path
        self.gamma = gamma
        self.epsilon = epsilon
        self.total_steps = 0
        self.epoch_gap = epoch_gap

    def select_action(self, state, is_test=False):
        action_values = self.action_values.get_state_action_values(state)

        if not is_test:
            indicator = np.random.uniform(0, 1)
            if indicator < self.epsilon:
                action = np.random.randint(self.env.num_actions)
                # print("action={}".format(action))
                return action
            else:
                return np.argmax(action_values)
        else:
            return np.argmax(action_values)

    def get_learning_rate(self):
        return (self.num_episodes - self.total_steps) / self.num_episodes * self.alpha

    def train(self):
        for e in range(self.num_episodes):
            print("In epoch {}".format(e))
            state = self.env.reset()
            self.total_steps += 1
            alpha = self.get_learning_rate()

            for t in range(self.episode_length):
                # print("At time step {}".format(t))
                action = self.select_action(state)
                next_state, reward, done = self.env.step(action)

                # update action-value
                state_action_value = self.action_values.get_state_action_value(state, action)
                next_state_action_values = self.action_values.get_state_action_values(next_state)
                target_value = reward + self.gamma * np.max(next_state_action_values)

                new_state_action_value = (1 - alpha) * state_action_value + alpha * target_value
                self.action_values.update_state_action_value(state, action, new_state_action_value)

                # update value
                state_value = self.values.get_state_value(state)
                next_state_value = self.values.get_state_value(next_state)
                target_value = reward + self.gamma * next_state_value
                new_state_value = (1 - alpha) * state_value + alpha * target_value
                self.values.update_state_value(state, new_state_value)

                if done:
                    break
                state = next_state
                if self.epoch_gap:
                    time.sleep(0.001)

            # save results every epoch
            self.save()

    def test(self, render=False):
        state = self.env.reset()
        while True:
            action = self.select_action(state, is_test=True)
            next_state, reward, done = self.env.step(action)
            if render:
                self.env.render()
            if done:
                break

    def save(self):
        self.values.save(self.path)
        self.action_values.save(self.path)

    def load(self):
        self.values.load(self.path)
        self.action_values.load(self.path)

    def get_value_table(self):
        return self.values.get_value_table()

    def get_action_value_table(self):
        return self.action_values.get_action_value_table()






