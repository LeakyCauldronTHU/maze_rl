import numpy as np
import subprocess
import threading
import time
import copy
from utils import cmd_render


class Maze:
    def __init__(self, num_states=16, num_actions=4, barrier_index=None):
        self.num_states = num_states
        self.num_actions = num_actions
        self.state_dims = np.sqrt(self.num_states).astype(np.int)
        if barrier_index is None:
            self.barrier_index = [(1, 1), (2, 2)]
            # self.barrier_index = [(1, 1), (2, 1), (2, 2), (3, 3)]
            # self.barrier_index = []
        else:
            self.barrier_index = barrier_index
        self.mouse_position = (self.state_dims-1, 0)
        self.curr_state = None
        self.first_render = True
        self.done = False
        self.cat_file = './render/cat_position.npy'
        self.mouse_file = './render/mouse_position.npy'
        self._save_env_config()

    def _save_env_config(self):
        barrier = np.array(self.barrier_index)
        np.save(file='./render/barrier_file.npy', arr=barrier)
        np.save(file='./render/grid_file.npy', arr=np.array([self.state_dims]))

    def _get_reward(self):
        if tuple(self.curr_state) in self.barrier_index:
            reward = -1
        elif tuple(self.curr_state) == self.mouse_position:
            reward = 1
        else:
            reward = -0.1
        return reward

    def _is_done(self):
        if tuple(self.curr_state) in self.barrier_index:
            done = True
        elif tuple(self.curr_state) == self.mouse_position:
            done = True
        else:
            done = False
        return done

    def reset(self):
        self.first_render = True
        self.curr_state = [0, self.state_dims-1]
        return self.curr_state

    def step(self, action):
        # 0: left; 1: up; 2: right; 3: down
        if action == 0:
            if self.curr_state[0] > 0:
                self.curr_state = [self.curr_state[0]-1, self.curr_state[1]]
        if action == 1:
            if self.curr_state[1] < self.state_dims - 1:
                self.curr_state = [self.curr_state[0], self.curr_state[1]+1]
        if action == 2:
            if self.curr_state[0] < self.state_dims - 1:
                self.curr_state = [self.curr_state[0]+1, self.curr_state[1]]
        if action == 3:
            if self.curr_state[1] > 0:
                self.curr_state = [self.curr_state[0], self.curr_state[1]-1]
        reward = self._get_reward()
        done = self._is_done()
        self.done = done
        return self.curr_state, reward, done

    def render(self, time_gap=0):
        time.sleep(time_gap)
        position = np.array([self.curr_state[0] * 100 + 50, self.curr_state[1] * 100 + 50])
        if self.done:
            np.save(file=self.cat_file, arr=position)
            time.sleep(1)
            np.save(file=self.cat_file, arr=np.array([]))
            self.th.join()

        np.save(file=self.cat_file, arr=position)
        if self.first_render:
            np.save(file=self.mouse_file, arr=np.array([self.mouse_position[0]*100+50, self.mouse_position[1]*100+50]))
            self.first_render = False
            self.th = threading.Thread(target=cmd_render, args=())
            self.th.start()


if __name__ == "__main__":
    env = Maze()
    state = env.reset()
    # actions = [2, 2, 2, 2, 2, 3,2, 3, 3]
    # actions = [2,2,3]
    # actions = [2,3,3]
    # actions = [3,3,3,2,1]
    # actions = [3,3,3,2,2,1,1]
    # actions = [3,2,1,2,3]
    # actions = [3,0,3,0,0,3,2,3,3,1]
    actions = [2,2,2,3,3,0,3,2]
    counter = 0
    while True:
        time.sleep(0.3)
        # action = np.random.randint(env.num_actions)
        action = actions[counter]
        counter += 1
        nxt_state, reward, done = env.step(action)
        if action == 0:
            action = 'left'
        if action == 1:
            action = 'up'
        if action == 2:
            action = 'right'
        if action == 3:
            action = 'down'

        state = copy.deepcopy(nxt_state)
        print("action", action, "state", state, "reward", reward)
        time.sleep(1)

        env.render()
        if done:
            break
