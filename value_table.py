import numpy as np
import os


class State_Values:
    def __init__(self, state_dims):
        self.state_dims = state_dims
        self.state_values = None
        self.reset()

    def reset(self):
        self.state_values = np.zeros([self.state_dims, self.state_dims])

    def update_state_value(self, state, value):
        self.state_values[state[0], state[1]] = value

    def get_state_value(self, state):
        return self.state_values[state[0], state[1]]

    def save(self, path):
        file = os.path.join(path, 'value_table.npy')
        np.save(file=file, arr=self.state_values)

    def load(self, path):
        file = os.path.join(path, 'value_table.npy')
        self.state_values = np.load(file=file)

    def get_value_table(self):
        return self.state_values


class State_Action_Values:
    def __init__(self, state_dims, num_actions):
        self.state_dims = state_dims
        self.num_actions = num_actions
        self.state_action_values = None
        self.reset()

    def reset(self):
        self.state_action_values = np.zeros([self.state_dims, self.state_dims, self.num_actions])

    def update_state_action_value(self, state, action, value):
        self.state_action_values[state[0], state[1], action] = value

    def get_state_action_value(self, state, action):
        return self.state_action_values[state[0], state[1], action]

    def get_state_action_values(self, state):
        return self.state_action_values[state[0], state[1]]

    def save(self, path):
        file = os.path.join(path, 'action_value_table.npy')
        np.save(file=file, arr=self.state_action_values)

    def load(self, path):
        file = os.path.join(path, 'action_value_table.npy')
        self.state_action_values = np.load(file=file)

    def get_action_value_table(self):
        return self.state_action_values

