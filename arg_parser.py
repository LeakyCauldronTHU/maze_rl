import argparse


def common_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--alg', help='rl algorithm, (q_learning, sarsa)', type=str, default='q_learning')
    parser.add_argument('--alpha', help='learning rate', type=float, default=0.01)
    parser.add_argument('--epsilon', help='epsilon greedy, random search with probability epsilon', type=float, default=0.3)
    parser.add_argument('--num-episodes', help='total episodes used for training', type=int, default=100000)
    parser.add_argument('--episode-length', help='maximum length of an episode', type=int, default=100)
    parser.add_argument('--path', help='path to save trained value table and q table', type=str, default='./data/')
    parser.add_argument('--visualize', help='visualize the value table and q table during training', default=False, action='store_true')
    parser.add_argument('--save-video', help='whether to save recorded video during training', default=False, action='store_true')
    parser.add_argument('--gamma', help='discount factor', type=float, default=0.99)
    parser.add_argument('--evaluate', help='in evaluate mode?', default=False, action='store_true')
    return parser


if __name__ == "__main__":
    pass

