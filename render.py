from mttkinter import mtTkinter as tk
import threading
import copy
import numpy as np
import time


class Render:
    def __init__(self, cat_position=None, mouse_position=None):
        self.game_width = 400
        self.game_height = 400
        self.master = tk.Tk()
        self.master.title("Cat and Mouse")
        self.master.geometry('%dx%d' % (self.game_width, self.game_height))
        # 禁止改变窗口大小
        self.master.resizable(width=False, height=False)
        self.cat_position = cat_position
        self.mouse_position = mouse_position
        self.prev_cat_position = None
        self.cat_file = './render/cat_position.npy'
        self.mouse_file = './render/mouse_position.npy'
        self.timer_gap = 0.0001
        self.barrier_file = './render/barrier_file.npy'
        self.grid_file = './render/grid_file.npy'
        barrier_index = list(np.load(file=self.barrier_file).astype(np.int))
        self.barrier_index = [list(i) for i in barrier_index]
        self._init()
        self.master.mainloop()

    def _init(self):
        self.grid = np.load(file=self.grid_file).astype(np.int)[0]
        self.cv = tk.Canvas(self.master, background='white', width=self.game_width, height=self.game_height)
        for r in range(self.grid):
            for c in range(self.grid):
                grid_position = [r * 100, c * 100]
                inx = [r, c]
                if inx in self.barrier_index:
                    self.cv.create_rectangle(grid_position[0], self.grid*100-grid_position[1], grid_position[0]+100,
                                             self.grid*100-grid_position[1]-100, fill='darkblue', stipple='gray25')
                else:
                    self.cv.create_rectangle(grid_position[0], self.grid*100-grid_position[1], grid_position[0]+100,
                                             self.grid*100-grid_position[1]-100)

        self.cv.pack()
        self.cv.focus_set()
        self.cat_img = tk.PhotoImage(file='./render/cat.gif')
        self.mouse_img = tk.PhotoImage(file='./render/mouse.gif')
        self.cat = self.cv.create_image(self.cat_position[0]*100+50, self.grid*100-self.cat_position[1]*100-50, image=self.cat_img)
        self.mouse = self.cv.create_image(self.mouse_position[0]*100+50, self.grid*100-self.mouse_position[1]*100-50, image=self.mouse_img)

        self.th = threading.Timer(self.timer_gap, self.move)
        self.th.start()

    def move(self):
        try:
            self.cat_position = list(np.load(file=self.cat_file))
            self.mouse_position = list(np.load(file=self.mouse_file))
        except:
            pass

        if not self.cat_position:
            self.master.quit()
        else:
            if self.cat_position == self.mouse_position:
                self.cv.coords(self.mouse, self.mouse_position[0] + 100, self.grid * 100 - self.mouse_position[1] - 100)

            if self.cat_position == self.prev_cat_position:
                self.th = threading.Timer(self.timer_gap, self.move)
                self.th.start()
            else:
                self.prev_cat_position = copy.deepcopy(self.cat_position)
                self.cv.coords(self.cat, self.cat_position[0], self.grid*100-self.cat_position[1])
                cat_grid_position = [self.cat_position[0]//100, self.cat_position[1]//100]
                if cat_grid_position in self.barrier_index:
                    time.sleep(0.5)
                    self.cv.coords(self.cat, self.grid*100, self.grid*100+100)

                self.th = threading.Timer(self.timer_gap, self.move)
                self.th.start()


if __name__ == "__main__":
    cat_position = [0, 3]
    mouse_position = [3, 0]
    Render(cat_position, mouse_position)
