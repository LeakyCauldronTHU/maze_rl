from mttkinter import mtTkinter as tk
import threading
import copy
import numpy as np
from utils import ten2hex


class Visualizer:
    def __init__(self, table=None, table_file=None):
        self.game_width = 400
        self.game_height = 400
        self.master = tk.Tk()
        if 'action' in table_file:
            self.master.title("Action-Value Table")
        else:
            self.master.title("Value Table")

        self.master.geometry('%dx%d' % (self.game_width, self.game_height))
        # 禁止改变窗口大小
        self.master.resizable(width=False, height=False)
        self.table = table
        self.prev_table = None
        self.table_file = table_file

        self.timer_gap = 0.0001
        self.barrier_file = './render/barrier_file.npy'
        self.grid_file = './render/grid_file.npy'
        self.terminate_indicator_file = './render/terminate_file.npy'
        self.terminate_indicator = 0.0
        barrier_index = list(np.load(file=self.barrier_file).astype(np.int))
        self.barrier_index = [list(i) for i in barrier_index]
        self._init()
        self.master.mainloop()

    def _init(self):
        self.grid = np.load(file=self.grid_file).astype(np.int)[0]
        self.cv = tk.Canvas(self.master, background='white', width=self.game_width, height=self.game_height)

        self.cv.pack()
        self.cv.focus_set()
        if 'action' in self.table_file:
            # 这里要创建的是三角形
            self.rectangles = [[[[],[],[],[]] for _ in range(self.grid)] for _ in range(self.grid)]
            for r in range(self.grid):
                for c in range(self.grid):
                    for e in range(4):
                        grid_position = [r * 100, c * 100]
                        inx = [r, c]
                        if inx not in self.barrier_index:
                            points0 = [grid_position[0], self.grid * 100 - grid_position[1],
                                       grid_position[0], self.grid * 100 - (grid_position[1] + 100),
                                       grid_position[0] + 50, self.grid * 100 - (grid_position[1] + 50)]
                            self.rectangles[r][c][0] = self.cv.create_polygon(points0)

                            points1 = [grid_position[0], self.grid * 100 - grid_position[1],
                                       grid_position[0] + 100, self.grid * 100 - grid_position[1],
                                       grid_position[0] + 50, self.grid * 100 - (grid_position[1] + 50)]
                            self.rectangles[r][c][1] = self.cv.create_polygon(points1)

                            points2 = [grid_position[0] + 100, self.grid * 100 - grid_position[1],
                                       grid_position[0] + 50, self.grid * 100 - (grid_position[1] + 50),
                                       grid_position[0] + 100, self.grid * 100 - (grid_position[1] + 100)]
                            self.rectangles[r][c][2] = self.cv.create_polygon(points2)

                            points3 = [grid_position[0], self.grid * 100 - (grid_position[1] + 100),
                                       grid_position[0] + 50, self.grid * 100 - (grid_position[1] + 50),
                                       grid_position[0] + 100, self.grid * 100 - (grid_position[1] + 100)]
                            self.rectangles[r][c][3] = self.cv.create_polygon(points3)
        else:
            self.rectangles = [[[] for _ in range(self.grid)] for _ in range(self.grid)]
            for r in range(self.grid):
                for c in range(self.grid):
                    grid_position = [r * 100, c * 100]
                    inx = [r, c]
                    if inx not in self.barrier_index:
                        self.rectangles[r][c] = self.cv.create_rectangle(
                            grid_position[0], self.grid * 100 - grid_position[1], grid_position[0] + 100,
                            self.grid * 100 - grid_position[1] - 100)

        self.th = threading.Timer(self.timer_gap, self.update)
        self.th.start()

    def update(self):
        try:
            self.prev_table = copy.deepcopy(self.table)
            self.table = np.load(file=self.table_file)
            self.terminate_indicator = np.load(file=self.terminate_indicator_file)
            # print("loaded file", self.table)
            # print("loaded", self.terminate_indicator)
        except:
            print("in except")
            pass

        if self.terminate_indicator:
            self.master.quit()
        else:
            # print("check1", self.table)
            # print("check2", self.prev_table)
            if (self.prev_table is None and self.table is not None) or (self.table == self.prev_table).all():
                # print("-----loop-------", self.prev_table)
                self.th = threading.Timer(self.timer_gap, self.update)
                self.th.start()
            else:
                # self.prev_table = copy.deepcopy(self.table)
                # print("check type", np.shape(self.table))
                # exit(0)
                if 'action' in self.table_file:
                    for r in range(self.grid):
                        for c in range(self.grid):
                            inx = [r, c]
                            if inx not in self.barrier_index:
                                for e in range(4):
                                    action_value = (self.table[r, c, e] + 1) / 2
                                    self.cv.itemconfig(self.rectangles[inx[0]][inx[1]][e], fill=ten2hex(action_value))

                else:
                    for r in range(self.grid):
                        for c in range(self.grid):
                            inx = [r, c]
                            if inx not in self.barrier_index:
                                value = self.table[r, c]
                                self.cv.itemconfig(self.rectangles[r][c], fill=ten2hex(value))

                self.th = threading.Timer(self.timer_gap, self.update)
                self.th.start()


if __name__ == "__main__":
    table = None
    file = './data/action_value_table.npy'
    Visualizer(table, file)
