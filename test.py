from tkinter import *
from utils import ten2hex

root = Tk()
count = 0
lcount = 5
color = []
for i in range(21):
    color.append(ten2hex(i/20))

print("color", color)
i = 0
for each_color in color:
    Label(text=each_color, bg=each_color).grid(row=int(i/lcount), column=i%lcount, sticky=W+E+N+S)
    i += 1

root.mainloop()