import numpy as np
import subprocess


def cmd_render():
    command = 'python render.py'
    subprocess.run(command, shell=True)


def cmd_visualize():
    command = 'python visualize.py'
    subprocess.run(command, shell=True)


def ten2hex(value):
    # print("value", value)
    ten = int(value * 255)
    hex_value = hex(ten)[2:]
    if len(hex_value) == 1:
        hex_value = '0' + hex_value
    return '#' + hex_value + '0000'



